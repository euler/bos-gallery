<?php

include_once "php/lib.php";
//session_start();

function toJson( $result ) {
	$resultArray = $result->fetch_all(MYSQLI_ASSOC);
	//$resultArray['loggedIn'] = isLoggedIn();
	return json_encode($resultArray);
}

function action( $action ) {
   switch( $action ) {
       case "addFile":
		$file = filter_var( $_REQUEST['file'], FILTER_SANITIZE_STRING);
		return  json_encode( addFile( $file ) );
       case "getPicturesForTopic":
		$topic = filter_var( $_REQUEST['topic'], FILTER_SANITIZE_STRING);
		if( preg_match( '/^([1-5])stars/', $topic, $treffer ) )  {
			return  json_encode( getPicturesForRating( $treffer[1] ) );			
		} else {
			return  json_encode( getPicturesForTopic( $topic ) );
		}
      case "rating": 
		$pid     = @$_POST['ct_pid'];      // PID from the form
        	$rating  = @$_POST['ct_rating'];   // Rating from the form
		$captcha = @$_POST['captcha_code'];
		if ($captcha != $_SESSION['captcha_spam']) {
			return json_encode( array("Error" => "wrong code $captcha != " . $_SESSION['captcha_spam']) );
		}
		return json_encode( updateVote( $pid, $rating ) );
       case "newComment": 
		$pid     = @$_POST['ct_pid'];      // PID from the form
		$comment = filter_var( $_REQUEST['comment'], FILTER_SANITIZE_STRING);
 		return json_encode( newComment($pid, $comment) );
       case "compile": 
		$code = $_REQUEST['code'];
		file_put_contents("snippet.java", $code );
		$cmd = 'java -cp "jserver.jar" jserver.JtoJS'; 
		//$cmd = "javac -verbose *.java"; 
		//$result["cmd"] = $cmd;
 		exec( $cmd, $result, $v );
		
 		return json_encode( $result );
       
       default:
    		return json_encode( array("Error" => "unknown request: " .  $action ) );
	
    }
}

session_start(); 
if(!isset($_SESSION["loggedIn"])){
	$_SESSION["loggedIn"] = false;
} 

if( isset(  $_REQUEST['action'] ) ) {
    $action = filter_var( $_REQUEST['action'], FILTER_SANITIZE_STRING);
    $content = action( $action );
 
} else {
    $app_info = array(
        "appName" => "BoS Gallery Manager", 
	"apiVersion" => "0.1"); 

    $content= json_encode($app_info); 
} 

echo $content;
?> 
