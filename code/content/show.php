<div class="pull-right" style="margin-top:-30px;">
<button type="button" class="btn btn-default btn-lg" onclick="sortImages( compareName )">
	<span class="glyphicon glyphicon-sort-by-alphabet"></span></button>
<button type="button" class="btn btn-default btn-lg" onclick="sortImages( compareRating )"> 
	<span class="glyphicon glyphicon-sort-by-order-alt"></button>
<button type="button" class="btn btn-default btn-lg" onclick="sortImages( compareRatingInv )"> 
	<span class="glyphicon glyphicon-sort-by-order"></button>

<form id="timeSelect"></form>
</div>

<?php
$topic = filter_var( $_REQUEST['topic'], FILTER_SANITIZE_STRING); 
echo "<h2>$topic</h2>";
//$pictures  = getPicturesForTopic( $topic );
echo "<h3> <span id='imageCount'></span> Bilder</h3>";
$i = 0;
?>

<div id="etiketten" > </div>
<div id="lightbox" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="margin-top:120px;">
       <button type="button" class="close hidden" data-dismiss="modal" aria-hidden="true">X</button>
        <div class="modal-content">
            <div class="modal-body">
                <img id="lbImage" src="" alt="" />
            </div>
    <a class="prev" onclick="nextSlide(-1)">&#10094;</a>
    <a class="next" onclick="nextSlide(1)">&#10095;</a>

    <div class="caption-container">
      <p id="caption"></p>
    </div>
        </div>
    </div>
</div>

<div id="ratingForm" class="hidden">
<form action="" method="post" id="rating_form" onsubmit="return processMyForm();">
	<input type="hidden" name="action" value="rating">
	<input type="hidden" name="ct_pid" id="form_pid" value="++">
	<select id="drpdwn" name="ct_rating" size="1" required>
		<option></option>
		<?php
			for( $i=1; $i<=5; $i++ ) {
				echo '<option value="' . $i . '">';
					for( $k=0; $k<$i; $k++ ) {
						echo '&#10032;';
					}
				echo '</option>';
			}
		?>
	</select>
	<img src="php/captcha/captcha.php?RELOAD=" id="captcha_pic" alt="Captcha" title="Klicken, um das Captcha neu zu laden" onclick="this.src+=1;document.getElementById('captcha_code').value='';" width=140 height=40 />
	<input type="text" name="captcha_code" id="captcha_code" size=10 required/>
	<input id="submitbtn" type="submit" value="Submit" />
</form>
</div>

<div id="editFormDiv" class="hidden">
<form action="" method="post" id="editForm" onsubmit="return newComment();">
	<input type="hidden" name="ct_pid" id="form_pid" value="++">
	<input type="hidden" name="action" value="newComment">
	<label>Kommentar:</label><input type="text" name="comment" id="comment" size=30 required/>
	<input id="submitbtn2" type="submit" value="Submit" />
</form>
</div>

<script>
var semesters = new Object()
var groups   = new Object()


function nextSlide( n ) {
        index += n;
	if ( index < 0 || index >= images.length  ) { 
		console.log("no more image " + index);
		index -= n
		return;
	}	

	var index2 = index;
	while( ! images[index2].show ) {
        	index2 += n;
		if ( index2 < 0 || index2 >= images.length  ) { 
			index -= n
			console.log("no more image to show " + index2);
			return;
		}
	}
	index = index2;

 	var im = document.getElementById( 'lbImage' );
	im.src = buildPath( images[ index ] )
	//alert( im.src );

        var captionP = $("#caption")
        var cont = (1. + index) + ". Bild " + ratingText( images[ index ] ) + " <div>" + images[ index ]['Comment'] + "</div>"

        var clonedDiv = $('#ratingForm').html()
	// unschoene Loesung mit String-Ersetzung zum Setzen der Picture-ID
        clonedDiv = clonedDiv.replace("++",  images[index]['PID'] );
	captionP.html( cont  )

        <?php
	if (isLoggedIn( )  ) {
		echo "var editDiv = $('#editFormDiv').html();";
        	echo "editDiv = editDiv.replace('++',  images[index]['PID'] );";
		echo "captionP.append( editDiv  )";
	}
	?>

	captionP.append( clonedDiv  )
}

function buildPath( image ) {
	return 'images/gallery/' + image['Path']  + '/' + image['Filename'] 
}

function ratingText( image ) {
 	if ( image.Votes == 0) {
    		return '<em>noch keine Bewertung</em>'
  	} else {
    		return Number(image.Rating / image.Votes).toFixed(2) + " aus " + image.Votes + " Stimmen";
	}
}

function buildPathThumb( image ) {
	filename = image['Filename']
        filename = filename.replace( /\.(jpe?g|png)$/i, "-thumb.png" )
	console.log( filename )
	return 'images/gallery/' + image['Path']  + '/' + filename
}

function sortImages( compare) {
	console.log("sortImages")
	images.sort( compare ) 
	showPreview()
}

function getRating( a ) {
	if( a.Votes == 0 ) return 0;
	else return a.Rating / a.Votes
}

// three compares:
// Rating
// if equal Votes
// if eqaul Name
function compareRating( a, b ) {
	var diff = getRating(b) - getRating(a)
        //console.log( a.Rating + " " + b.Rating + " " + diff )
	if( diff != 0 ) return diff
	var diffV = b.Votes - a.Votes
	if( diffV != 0 ) return diffV
	return compareName( a, b );
}

function compareRatingInv( a, b ) {
	return compareRating( b, a )
}	

function compareName(a, b) {
  if (a.Filename > b.Filename) {
    return 1;
  }
  if (a.Filename < b.Filename) {
    return -1;
  }
  return 0;
}

function showPreview() {
	var etDiv = $("#etiketten")
	var shown = 0
	etDiv.html("")      
        for( var i=0; i<images.length; i++ ) {
		if( semesters[ images[i].semester] && groups[ images[i].group] ) {
			++shown
			images[i].show = true
			etDiv.append( "<img src='" + buildPathThumb( images[i] ) + "'" 
				+ " data-toggle='modal' data-target='#lightbox'"
				+ " onmouseover='index = " + i + "; nextSlide( 0 )'"
				+ " class='img-thumbnail'"
				+ " style='width:60px'"
				+ ">")
		}  else {
			images[i].show = false
		}
	}
	$("#imageCount").html( shown + " aus " + images.length )
}

function parseAttributes() {
        for( var i=0; i<images.length; i++ ) {
		images[i].show   = true
		var atts = images[i].Path.split("\/")
		images[i].semester = atts[0]
		images[i].group  = atts[1]
		images[i].show   = true
		//console.log( images[i].Path +" " + atts[0])
		semesters[ images[i].semester ] = true
		groups[ images[i].group ] = true

	}
	console.log( semesters )
	console.log( groups )

	buildCheckBoxes( semesters, "timeCheck" )
	buildCheckBoxes( groups, "groupCheck" )

}

function buildCheckBoxes( list, className ) {
	var boxArray = [] 
	for( var elm in list ) {
		console.log( elm )
		boxArray.push( 
			'<label class="checkbox-inline">' + 
			'<input type="checkbox" class="' + className +'" value="'
			+ elm + '" checked>'
			+ elm + '</label>' )
	}
	$("#timeSelect").append( "<div>" + boxArray.sort() + "</dviv>" )
	$('.'+className).click(function( ) {
		//alert(  $(this).val() )
		var elm = $(this).val()
		list[ elm ] = !  list[ elm ];
		showPreview()
	})
}


function processMyForm() {
	//alert($('#rating_form').serialize())
	$.post("api.php", $('#rating_form').serialize(), function(data){
		//alert("Data: " + data);
		image = JSON.parse( data );
		if ( 'Error' in image) {
			alert("There was an error with your submission" )
		} else {
			// update rating for selected picture
		       for( var i=0; i<images.length; i++ ) {
				console.log( images[i]['PID'] + " " +  image[0]['PID'] )
				if( images[i]['PID'] == image[0]['PID'] ) {
					images[i].Rating = image[0].Rating
					images[i].Votes  = image[0].Votes
					nextSlide( 0 )
					break
				}
			}
		}
 	});
	return false;
}

function newComment() {
	$.post("api.php", $('#editForm').serialize(), function(data){
		//	console.log( $('#editForm').serialize() ) 
		image = JSON.parse( data );
		if ( 'Error' in image) {
			alert("There was an error with your submission" )
		} else {
			// update rating for selected picture
		       for( var i=0; i<images.length; i++ ) {
				console.log( images[i]['PID'] + " " +  image[0]['PID'] )
				if( images[i]['PID'] == image[0]['PID'] ) {
					images[i].Rating = image[0].Rating
					images[i].Comment  = image[0].Comment
					nextSlide( 0 )
					break
				}
			}
		}
 	});
	return false;
}

$(document).ready(function() {
    	var $lightbox = $('#lightbox');
    
    	topic = "<?php echo $topic;?>";
 	action = "getPicturesForTopic"
        //alert( typ + " : " + action );
	$.get("api.php", {action:action, topic:topic }, function(data){
  		//alert("Data: " + data);
      		var  css = {
                	'maxWidth': $(window).width() - 300,
                	'maxHeight': $(window).height() - 300
            	};
        	images = JSON.parse( data );
		if( images.length == 1 ) {
			$(".prev, .next").hide();
  		} else {
			$(".prev, .next").show();
		}
		parseAttributes()
 		showPreview();
		index = 0,
        	nextSlide( 0 );
        	$lightbox.find('.close').addClass('hidden');
        	$lightbox.find('img').css(css);
 	});
    
    $lightbox.on('shown.bs.modal', function (e) {
        var $img = $lightbox.find('img');
            
        //$lightbox.find('.modal-dialog').css({'width': $img.width()});
        $lightbox.find('.close').removeClass('hidden');
    });

});
</script>