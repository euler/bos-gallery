<?php
$name       = filter_var( $_REQUEST['name'], FILTER_SANITIZE_STRING);
$password   = filter_var( $_REQUEST['password'], FILTER_SANITIZE_STRING);
?>

<div class="col-md-1"></div>
<div class="col-md-9">
<?php
echo "<div>Benutzer $name </div>";

if( loginUser( $name, $password ) ) {
	echo "Login okay";
	$_SESSION["loggedin"] = true;
	echo '<script>';
	echo '$("#loginIcon").removeClass( "glyphicon-log-in" ).addClass( "glyphicon-log-out" )';
	echo '</script>';
} else {
	echo "Login nicht m&ouml;glich";
}
?>

</div>
