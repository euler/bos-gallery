BoS Galerie Version 2.0 vom Dezember 2017

<p>Diese  Web-Applikation nutzt mehrere freie Bibliotheken und Module, die nachfolgend aufgef&uuml;hrt sind.</p>
			<p>jQuery</p>
				<p><a href="https://jquery.com" target="_blank">https://jquery.com</p></a>
			<p>Bootstrap</p>
				<p><a href="https://getbootstrap.com" target="_blank">https://getbootstrap.com</p></a>
			<p>Captcha-Modul von WWW Coding</p>
				<p><a href="https://www-coding.de/so-gehts-eigenes-captcha-mit-php/" target="_blank">https://www-coding.de/so-gehts-eigenes-captcha-mit-php/</p></a>

Bei der Realisierung haben mitgewirkt:
<ul>
<li>Ibrahim Nasim: erste Version mit Datenbank, Bewertung und Filterung</li>
</ul>