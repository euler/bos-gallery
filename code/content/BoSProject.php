<div class="row"> 
<div class="col-md-9 col-md-offset-1">

Auf dieser Web-Site sind Bilder zusammengestellt, die in verschiedenen Lehrveranstaltungen 
mittels <em>Board of Symbols</em> (BoS) erstellt wurden. 
Mit BoS kann man &uuml;ber einfache Befehle Muster erzeugen. 
Zu wird beispielsweise mit dem Code-Abschnitt 
<pre><code>for( x=1; x&lt;6;  x++ ) {
  for( y=x; y&lt;6; y++ ) {
    farbe2( x, y, BLUE )
    if( x == y ) {
      form2( x, y, "tlu" )
    } else {
      form2( x, y, "s" )
   }
  }
}
</code></pre>
das Bild 
<div><img src="images/BoS.PNG" alt="BoS Screen" style="max-width:100%;"></div>
erzeugt.
Informationen zum BoS-Projekt findet man auf <a href="https://github.com/stephaneuler/board-of-symbols">github</a>

</div>
</div>